function getAll() {
	$.ajax({
		url : "http://localhost:8585/url/",
		type : "POST",
		dataType : "json"
	}).then(function(data) {
		console.log(data);
		$('#result').text(data);

	});
}

function createShortUrl() {
	var completeUrl = $('#completeUrl').val();
	console.log(completeUrl);
	$.ajax({
		url : "http://localhost:8585/url/create/",
		type : "POST",
		data : {
			"completeUrl" : completeUrl
		},
	}).then(function(data) {
		$('#result').text(data);

	});
}

function getByShortUrl() {
	var shortUrl = $('#shortUrl').val();
	$.ajax({
		url : "http://localhost:8585/url/getByShort/",
		type : "GET",
		data : {
			"shortUrl" : shortUrl
		},
	}).then(function(data) {
		$('#result').text(data);

	});
}