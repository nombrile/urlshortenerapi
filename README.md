# README #

### Url Shortener ###

* Version 1.0

### How do I get set up? ###

* Download project
* Run as Spring Boot App
* open browser to http://localhost:8585

### Precisions ###
- You can change port in application.properties (server.port)
- You can change database location in application.properties (spring.datasource.url)