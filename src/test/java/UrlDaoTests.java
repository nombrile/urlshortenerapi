import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.support.AnnotationConfigContextLoader;

import junit.framework.Assert;
import urlshortener.constants.UrlShortenerConstants;
import urlshortener.dao.UrlDAO;
import urlshortener.domain.UrlObject;
import urlshortener.domain.impl.UrlObjectImpl;

@RunWith(SpringRunner.class)
@DataJpaTest
@ContextConfiguration(loader = AnnotationConfigContextLoader.class)
@EnableAutoConfiguration
@ComponentScan("urlshortener")
public class UrlDaoTests {

	@Autowired
	private TestEntityManager entityManager;

	@Autowired
	private UrlDAO urlDao;

	@Configuration
	static class ContextConfiguration {

	}

	@Test
	public void findUrlByShortUrl() {

		UrlObjectImpl newShortUrl = new UrlObjectImpl("www.monSite.ca", UrlShortenerConstants.DOMAIN + "5h8jW");

		entityManager.persist(newShortUrl);
		entityManager.flush();

		UrlObject foundUrl = urlDao.getUrlObjectByShortUrl(newShortUrl.getShortUrl());

		Assert.assertEquals(newShortUrl, foundUrl);
	}

	@Test
	public void findUrlByCompleteUrl() {

		UrlObjectImpl newShortUrl = new UrlObjectImpl("www.monSite.ca", UrlShortenerConstants.DOMAIN + "5h8jW");

		entityManager.persist(newShortUrl);
		entityManager.flush();

		UrlObject foundUrl = urlDao.getUrlObjectByCompleteUrl(newShortUrl.getCompleteUrl());

		Assert.assertEquals(newShortUrl, foundUrl);
	}

}
