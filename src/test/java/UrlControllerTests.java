import static org.mockito.BDDMockito.given;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import urlshortener.constants.UrlShortenerConstants;
import urlshortener.controller.UrlController;
import urlshortener.domain.UrlObject;
import urlshortener.domain.impl.UrlObjectImpl;
import urlshortener.service.UrlService;

@RunWith(SpringRunner.class)
@WebMvcTest(UrlController.class)
public class UrlControllerTests {

	private static final String shortUrlKey = "5h8jW";

	@Configuration
	static class TestConfiguration {
		@Bean
		public UrlController urlController() {
			return new UrlController();
		}
	}

	@Autowired
	private MockMvc mvc;

	@MockBean
	private UrlService urlService;

	@Test
	public void getCompleteUrlFromShortUrlTest() throws Exception {
		String completeUrlForTest = "www.monSite.ca";
		UrlObject newShortUrl = new UrlObjectImpl(completeUrlForTest, UrlShortenerConstants.DOMAIN + shortUrlKey);

		given(urlService.getUrlObjectByShortUrl(newShortUrl.getShortUrl())).willReturn(newShortUrl);

		mvc.perform(get("/url/getByShort/").accept(MediaType.ALL).param("shortUrl", newShortUrl.getShortUrl()))
				.andExpect(status().isOk()).andExpect(content().string(newShortUrl.getCompleteUrl()));

	}

	@Test
	public void createShortUrlTest() throws Exception {
		String completeUrlForTest = "www.monSite.ca";
		UrlObject newShortUrl = new UrlObjectImpl(completeUrlForTest, UrlShortenerConstants.DOMAIN + shortUrlKey);

		given(urlService.createNewShortUrl(newShortUrl.getCompleteUrl())).willReturn(newShortUrl);

		mvc.perform(post("/url/create/").accept(MediaType.ALL).param("completeUrl", newShortUrl.getCompleteUrl()))
				.andExpect(status().isOk()).andExpect(content().string(newShortUrl.getShortUrl()));

	}
}
