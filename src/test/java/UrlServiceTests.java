import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Bean;
import org.springframework.test.context.junit4.SpringRunner;

import junit.framework.Assert;
import urlshortener.constants.UrlShortenerConstants;
import urlshortener.dao.UrlDAO;
import urlshortener.domain.UrlObject;
import urlshortener.domain.impl.UrlObjectImpl;
import urlshortener.service.UrlService;
import urlshortener.service.impl.UrlServiceImpl;

@RunWith(SpringRunner.class)
public class UrlServiceTests {

	private static final String shortUrlKey = "5h8jW";

	@TestConfiguration
	static class UrlServiceImplTestContextConfiguration {

		@Bean
		public UrlService employeeService() {
			return new UrlServiceImpl();
		}
	}

	@Autowired
	private UrlService urlService;

	@MockBean
	private UrlDAO urlDao;

	@Before
	public void setUp() {
		UrlObjectImpl newShortUrl = new UrlObjectImpl("www.monSite.ca", UrlShortenerConstants.DOMAIN + shortUrlKey);

		Mockito.when(urlDao.getUrlObjectByCompleteUrl(newShortUrl.getCompleteUrl())).thenReturn(newShortUrl);

		Mockito.when(urlDao.getUrlObjectByShortUrl(newShortUrl.getShortUrl())).thenReturn(newShortUrl);

	}

	@Test
	public void findUrlByCompleteUrl() {
		String completeUrl = "www.monSite.ca";
		UrlObject foundUrl = urlService.getUrlObjectByCompleteUrl(completeUrl);

		Assert.assertEquals(completeUrl, foundUrl.getCompleteUrl());
	}

	@Test
	public void findUrlByShortUrl() {
		String shortUrl = UrlShortenerConstants.DOMAIN + shortUrlKey;
		UrlObject foundUrl = urlService.getUrlObjectByShortUrl(shortUrl);

		Assert.assertEquals(shortUrl, foundUrl.getShortUrl());
	}

}
