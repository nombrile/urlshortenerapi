package urlshortener.service;

import java.util.List;

import urlshortener.domain.UrlObject;

public interface UrlService {

	public boolean isShortUrlAlreadyExist(String completeUrl);

	public UrlObject getUrlObjectByCompleteUrl(String completeUrl);

	public UrlObject createNewShortUrl(String completeUrl);

	public UrlObject getUrlObjectByShortUrl(String shortUrl);

	public List<UrlObject> findAllUrls();

	public String generateKey();

}
