package urlshortener.service.impl;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import urlshortener.constants.UrlShortenerConstants;
import urlshortener.dao.UrlDAO;
import urlshortener.domain.UrlObject;
import urlshortener.domain.impl.UrlObjectImpl;
import urlshortener.service.UrlService;

@Service("urlService")
public class UrlServiceImpl implements UrlService {

	public static final Logger logger = Logger.getLogger(UrlServiceImpl.class);
	List<UrlObject> allUrls;

	@Autowired
	UrlDAO urlDao;

	@Override
	public List<UrlObject> findAllUrls() {
		logger.info("Entered service method findAllUrs()");
		List<UrlObject> allUrls = new ArrayList<UrlObject>();
		for (UrlObject urlObj : urlDao.findAll()) {
			allUrls.add(urlObj);
		}
		return allUrls;
	}

	@Override
	public boolean isShortUrlAlreadyExist(String completeUrl) {
		logger.info("Entered service method isShortUrlAlreadyExist(String completeUrl) with value: " + completeUrl);
		if (urlDao.getUrlObjectByCompleteUrl(completeUrl) != null) {
			return true;
		}
		return false;
	}

	@Override
	public UrlObject createNewShortUrl(String completeUrl) {
		logger.info("Entered service method createNewShortUrl(String completeUrl) with value: " + completeUrl);
		// if url has already been shortened
		if (isShortUrlAlreadyExist(completeUrl)) {
			return getUrlObjectByCompleteUrl(completeUrl);
		} else {
			// Generating shortUrl key
			String generatedKey = generateKey();
			if (generatedKey.length() <= 10) {
				String shortUrl = UrlShortenerConstants.DOMAIN + generatedKey;
				UrlObject createdUrlObject = urlDao.save(new UrlObjectImpl(completeUrl, shortUrl));
				return createdUrlObject;
			} else {
				logger.info("No short url generated, all short key with 10 or less characters are taken");
				return null;
			}

		}
	}

	@Override
	public String generateKey() {
		logger.info("Entered service method generateKey()");
		Iterable<UrlObjectImpl> urlIterable = urlDao.findAll();

		long generatedId = ((Collection<UrlObjectImpl>) urlIterable).size();
		long remaining = generatedId;
		StringBuilder str = new StringBuilder();
		if (generatedId == 0) {
			str.insert(0, "a");
		}
		while (remaining > 0) {
			str.insert(0, UrlShortenerConstants.ALPHABET[(int) (remaining % UrlShortenerConstants.BASE)]);
			remaining = remaining / UrlShortenerConstants.BASE;
		}

		return str.toString();
	}

	// Alternative way to generate key
	// @Override
	// public String generateKey(){
	// Random random = new Random();
	// String shortKey = "";
	// boolean alreadyTaken = true;
	// while(alreadyTaken){
	// int shortKeyLength =
	// random.nextInt(UrlShortenerConstants.MAX_LENGTH_OF_URL_CODE) + 1;
	// for(int i = 0; i < shortKeyLength; i++){
	// shortKey +=
	// UrlShortenerConstants.ALPHABET[random.nextInt(UrlShortenerConstants.BASE)];
	// }
	//
	// if(getUrlObjectByShortUrl(UrlShortenerConstants.DOMAIN+shortKey) ==
	// null){
	// alreadyTaken = false;
	// } else{
	// shortKey = "";
	// }
	// }
	//
	// return shortKey;
	// }

	@Override
	public UrlObject getUrlObjectByCompleteUrl(String completeUrl) {
		logger.info("getUrlObjectByCompleteUrl(String completeUrl) with value: " + completeUrl);
		UrlObject retrievedUrl = urlDao.getUrlObjectByCompleteUrl(completeUrl);
		return retrievedUrl;
	}

	@Override
	public UrlObject getUrlObjectByShortUrl(String shortUrl) {
		logger.info("Entered service method getUrlObjectByShortUrl(String shortUrl) with value: " + shortUrl);
		UrlObject retrievedUrl = urlDao.getUrlObjectByShortUrl(shortUrl);
		return retrievedUrl;
	}

}
