package urlshortener.domain;

public interface UrlObject {

	public String getCompleteUrl();

	public String getShortUrl();
}
