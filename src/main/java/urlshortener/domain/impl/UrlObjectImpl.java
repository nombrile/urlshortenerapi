package urlshortener.domain.impl;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import urlshortener.domain.UrlObject;

@Entity(name = "shorturl")
@Table(name = "SHORTURL")
public class UrlObjectImpl implements UrlObject {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;

	@Column
	private String completeUrl;

	@Column
	private String shortUrl;

	public UrlObjectImpl() {

	}

	public UrlObjectImpl(String completeUrl, String shortUrl) {
		this.completeUrl = completeUrl;
		this.shortUrl = shortUrl;
	}

	@Override
	public String getCompleteUrl() {
		return completeUrl;
	}

	public void setCompleteUrl(String completeUrl) {
		this.completeUrl = completeUrl;
	}

	@Override
	public String getShortUrl() {
		return shortUrl;
	}

	public void setShortUrlKey(String shortUrl) {
		this.shortUrl = shortUrl;
	}

}
