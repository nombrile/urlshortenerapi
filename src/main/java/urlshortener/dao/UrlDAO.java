package urlshortener.dao;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import urlshortener.domain.UrlObject;
import urlshortener.domain.impl.UrlObjectImpl;

@Repository
public interface UrlDAO extends CrudRepository<UrlObjectImpl, Long> {
	
	@Query("select u from shorturl u where u.completeUrl=:completeUrl")
	public UrlObject getUrlObjectByCompleteUrl(@Param("completeUrl") String completeUrl);

	@Query("select u from shorturl u where u.shortUrl=:shortUrl")
	public UrlObject getUrlObjectByShortUrl(@Param("shortUrl") String shortUrl);

}
