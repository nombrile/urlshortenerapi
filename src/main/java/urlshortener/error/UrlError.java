package urlshortener.error;

public class UrlError {
	private String errorMessage;
	 
    public UrlError(String errorMessage){
        this.errorMessage = errorMessage;
    }
 
    public String getErrorMessage() {
        return errorMessage;
    }
}
