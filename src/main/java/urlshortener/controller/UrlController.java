package urlshortener.controller;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.validator.routines.UrlValidator;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import urlshortener.domain.UrlObject;
import urlshortener.error.UrlError;
import urlshortener.service.UrlService;

@RestController
@RequestMapping("/")
public class UrlController {

	public static final Logger logger = Logger.getLogger(UrlController.class);

	@Autowired
	UrlService urlService;

	@RequestMapping(value = "/url/", method = RequestMethod.GET)
	public ResponseEntity<?> getUrls() {
		List<UrlObject> urlsList = new ArrayList<UrlObject>();
		urlsList = urlService.findAllUrls();

		if (urlsList.isEmpty()) {
			return new ResponseEntity<String>(new UrlError("No url found").getErrorMessage(), HttpStatus.OK);
		}

		List<String> shortUrlsList = new ArrayList<String>();
		for (UrlObject urlObj : urlsList) {
			shortUrlsList.add(urlObj.getShortUrl());
		}

		return new ResponseEntity<List<String>>(shortUrlsList, HttpStatus.OK);
	}

	@RequestMapping(value = "/url/create/", method = RequestMethod.POST)
	public ResponseEntity<?> createShortUrl(@RequestParam("completeUrl") String completeUrl) {
		logger.info("Creating short url from complete url: " + completeUrl);
		UrlValidator urlValidator = new UrlValidator();
		if (StringUtils.isEmpty(completeUrl) || !urlValidator.isValid(completeUrl)) {
			return new ResponseEntity<String>(new UrlError("URL invalide, veuillez recommencer").getErrorMessage(),
					HttpStatus.ACCEPTED);
		}

		if (urlService.isShortUrlAlreadyExist(completeUrl)) {
			logger.info("Url: " + completeUrl + "has already been shortened");
			UrlObject urlRetrieved = urlService.getUrlObjectByCompleteUrl(completeUrl);
			return new ResponseEntity<String>(urlRetrieved.getShortUrl(), HttpStatus.OK);
		}
		UrlObject newShortenedUrl = urlService.createNewShortUrl(completeUrl);
		if (newShortenedUrl == null) {
			return new ResponseEntity<String>(new UrlError("Aucun URL raccourci restant").getErrorMessage(),
					HttpStatus.ACCEPTED);
		}
		return new ResponseEntity<String>(newShortenedUrl.getShortUrl(), HttpStatus.OK);
	}

	@RequestMapping(value = "/url/getByShort/", method = RequestMethod.GET)
	public ResponseEntity<?> getUrlByShortKey(@RequestParam("shortUrl") String shortUrl) {
		logger.info("Getting url object associated with short url value: " + shortUrl);
		if (StringUtils.isEmpty(shortUrl)) {
			return new ResponseEntity<String>(new UrlError("Aucun URL entré, veuillez recommencer").getErrorMessage(),
					HttpStatus.ACCEPTED);
		}
		UrlObject retrievedUrl = urlService.getUrlObjectByShortUrl(shortUrl);
		if (retrievedUrl == null) {
			logger.error("No url found with shortened url: " + shortUrl);
			return new ResponseEntity<String>(
					new String("Aucun url complet ne correspond a l'url raccourci: " + shortUrl), HttpStatus.ACCEPTED);
		}
		return new ResponseEntity<String>(retrievedUrl.getCompleteUrl(), HttpStatus.OK);
	}

}
